/* Control interface for the EIDBalance widget.

    This demo widget uses inputs scaled 0 .. 1. The accompanying SVG
    file has 0 .. 200 on the inflow and outflow axes and 0 .. 50 on the
    fill axis. It is a recreation of the mass balance for a cement 
    mill, the fill axis is in percent fill level.
    
    The origin point in the SVG graph is at the left bottom.

    To animate the fill level indication (the large colored block),
    it is scaled in x direction, with respect to the x0, y0 points.

    The indicator below the graph is translated, as is the numeric
    indication below it. The values of the numeric indication are
    also adjusted.

    The "funnel" line runs left-right. Initially in the SVG drawing
    it is running from 0 inflow to max outflow. To reflect the state
    of the balance, it is first scaled in vertical direction according
    to the outflow - inflow value (which may vary between 1 (max outflow
    0 inflow) and -1 (max inflow, no outflow)). This scaling then adjusts
    the funnel line slope. It is then translated to start at the current
    total inflow.
    
    the inflow and outflow bars are scaled in y (vertical direction)
    with respect to the y0 point. Inflow1 / outflow1 is scaled to their
    respective values, inflow2/outflow2 are scaled to the sum
    inflow and outflow values. Since the inflow1/outflow1 are drawn
    above the inflow2/outflow2, only the inflow2-inflow1 portion (and
    similar for 2) is visible. Less complicated than scaling+translating

    The fill level is colored to follow the colors in the target
    bar below. In a more dynamic graph, the targets might also change,
    this exercise is left to the readers.
*/
export interface EIDBalanceInterface {
    inflow1: number,  // First inflow
    inflow2: number,  // Second inflow
    outflow1: number, // First outflow
    outflow2: number, // Second outflow
    fill_level: number     // current level
}
