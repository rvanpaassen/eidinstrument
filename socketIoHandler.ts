// socketIoHandler.js
import { Server } from 'socket.io';
import type { EIDBalanceInterface } from './src/lib/eid-interface';

// test values for EID balance
var eid_balance: EIDBalanceInterface = {
  inflow1: 0.2,
  inflow2: 0.3,
  outflow1: 0.3,
  outflow2: 0.2,
  fill_level: 0.4
};


export default function injectSocketIO(server: any) {
  const io = new Server(server);

  io.on('connection', (socket) => {

    console.log("new websock connection")
    // time counter for EID balance stuff
    var t: number = 0;

    // simple time-based updating of the values, so we see some movement
    const interval = setInterval(() => {
      t = t + 0.1;

      eid_balance.inflow1 = 0.2 + 0.2 * Math.sin(0.1 * t);
      eid_balance.inflow2 = 0.3 + 0.3 * Math.sin(0.22 * t + 0.3);
      eid_balance.outflow1 = 0.3 + 0.3 * Math.sin(0.24 * t + 1.5);
      eid_balance.outflow2 = 0.2 + 0.2 * Math.sin(0.0152 * t + 0.6);
      eid_balance.fill_level +=
        0.01 *
        (eid_balance.inflow1 + eid_balance.inflow2 - eid_balance.outflow1 - eid_balance.outflow2);

      // console.log(t, eid_balance)

      socket.emit('data', JSON.stringify(eid_balance))

    }, 1000);

    // and receive any target value changes
    socket.on('target', (val: any) => {
      console.log("Target value received", val)
      eid_balance.fill_level = val*0.01
    })

  });

  console.log('SocketIO injected');
}
