# EID balance drawing demo

This is a demo of an EID balance drawing. The data driving it is nonsense!
The balance has inflow on the left, outflow on the right, and fills up
left to right. The level has a target control on the bottom, in the form
of a slider symbol that can be moved left and right.

![EID Balance Drawing](demo.png)

This is intended as a proof-of-concept, it uses an SVG file created with
inkscape, and marked up with "class" labels. On the basis of the class
labels, a web application can find the SVG elements that need to be changed
to animate the graph. Example animation code is given in the EIDBalance.svelte
component.

To demonstrate communication, the application uses socket.io to communicate
data in JSON format. The bogus simulation data for the graph is received, and
any slider manipulation data is sent back.

## Further Development

Once you've cloned this project with git, use `npm install` to install
dependencies. Run a development server (from within VSCode, or from a terminal)
with:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview` .
